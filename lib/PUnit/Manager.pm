package PUnit::Manager;

use warnings;
use strict;

use PUnit::Logger qw(
    debug
    info
    error
    warning
);
use Data::Dumper qw(Dumper);

use Carp;

our $mgr;

sub new {
    my ($self, %args) = @_;
    my $class = ref $self ? ref $self : $self;
    $mgr = bless( \%args, $class );
}

sub run {
    my ($self, %args) = @_;

    if ($self->is_debug()) {
        info "Debug is enabled...";
        debug("Manager instance: " . Dumper($self));
    }

    info "Running...";
}

sub get_manager {
    $mgr or croak("Manager instance doesn't exist");
}

sub is_debug {
    shift->{options}->{debug};
}

1;
