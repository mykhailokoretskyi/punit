package PUnit::Logger;

use warnings;
use strict;

use Exporter;
use Term::ANSIColor qw(:constants);
use PUnit::Manager;

use constant DEBUG => "DEBUG";
use constant INFO => "INFO";
use constant WARNING => "WARN";
use constant ERROR => "ERROR";


our @ISA = qw( Exporter );

our @EXPORT_OK = qw(
    debug
    info
    warning
    error
);

sub debug {
    return
        unless PUnit::Manager::get_manager()->is_debug();

    my $msg = shift;
    _print_out(CYAN(), DEBUG() . ": " . $msg);
}

sub info {
    my $msg = shift;
    _print_out(BRIGHT_GREEN(), INFO() . ": " . $msg);
}

sub warning {
    my $msg = shift;
    _print_out(BRIGHT_MAGENTA(), WARNING() . ": " . $msg);
}

sub error {
    my $msg = shift;
    _print_out(BRIGHT_RED(), ERROR() . ": " . $msg);
}

sub _print_out {
    my ($color, $message) = @_;
    my ($package, $filename, $line, $i);
    do {
        ($package, $filename, $line) = caller($i++);
    } while ($package eq __PACKAGE__);
    print $color, "$message - $package at line $line" . "\n", RESET();
}

1;
