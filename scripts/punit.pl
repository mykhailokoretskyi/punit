#!/usr/bin/perl -w

use Getopt::Long;
use Pod::Usage;

use PUnit::Manager;

use warnings;
use strict;

Getopt::Long::Configure("bundling");
#$Getopt::Long::debug = 1;

my %options;

GetOptions(
    \%options,
    'help|h',
    'verbose|v',
    'debug',
) or pod2usage(2);

pod2usage(
    -exitval => 0,
    -verbose => 1,
) if $options{help};

my $mgr = PUnit::Manager->new(
    options => \%options,
    argv => [ @ARGV ],
);
$mgr->run();

=pod

=head1 NAME

PUnit - Test Framework for Perl

=head1 SYNOPSIS

Here should be command line usage description

=cut
